import React, { Component } from "react";

const ProductContext = React.createContext();
const ProductsConsumer = ProductContext.Consumer;

class ProductsProvider extends Component {
  state = {
    carts: [],
    cartTotal: 0
  };

  componentDidMount = () => {
    if (localStorage.carts) {
      let cartProducts = JSON.parse(localStorage.carts);
      this.setState(() => {
        return {
          carts: cartProducts
        };
      }, this.updateCartState());
    }
  };

  addToCart = product => {
    let cartsInStorage = [];
    let tmpProduct = {
      product_id: product.product_id,
      name: product.name,
      image: product.image,
      price: product.price,
      count: 1,
      total: product.price
    };

    if (localStorage.carts) {
      cartsInStorage = JSON.parse(localStorage.carts);
      let item = cartsInStorage.find(p => p.product_id === product.product_id);
      if (item) {
        item.count++;
        item.total = parseInt(item.price * item.count);
      } else {
        cartsInStorage.push(tmpProduct);
      }
    } else {
      cartsInStorage.push(tmpProduct);
    }
    localStorage.setItem("carts", JSON.stringify(cartsInStorage));
    this.updateCartState();
  };

  addTotal = () => {
    let subTotal = 0;
    this.state.carts.map(item => (subTotal += item.total));
    const total = Math.floor(subTotal);
    this.setState(() => ({
      cartTotal: total
    }));
  };

  updateCartState = () => {
    this.setState(
      () => ({ carts: JSON.parse(localStorage.carts) }),
      () => this.addTotal()
    );
  };

  handleAddToCart = id => {
    this.addToCart(id);
  };

  handleIncrementItem = id => {
    let cartProducts = JSON.parse(localStorage.carts);
    let product = cartProducts.find(p => p.product_id === id);
    product.count++;
    product.total = Math.floor(product.count * product.price);

    localStorage.setItem("carts", JSON.stringify(cartProducts));
    this.updateCartState();
  };

  handleDecrementItem = id => {
    let cartProducts = JSON.parse(localStorage.carts);
    let product = cartProducts.find(p => p.product_id === id);
    product.count--;

    if (product.count === 0) {
      this.handleRemoveItem(id);
    } else {
      product.total = Math.floor(product.count * product.price);
      localStorage.setItem("carts", JSON.stringify(cartProducts));
      this.updateCartState();
    }
  };

  handleRemoveItem = id => {
    let cartProducts = JSON.parse(localStorage.carts);
    let tmpCarts = cartProducts.filter(p => p.product_id !== id);
    localStorage.setItem("carts", JSON.stringify(tmpCarts));
    this.updateCartState();
  };

  handleClearCart = () => {
    localStorage.removeItem("carts");
    this.setState(
      () => {
        return { carts: [] };
      },
      () => {
        this.addTotal();
      }
    );
  };

  render() {
    return (
      <ProductContext.Provider
        value={{
          ...this.state,
          handleAddToCart: this.handleAddToCart,
          incrementItem: this.handleIncrementItem,
          decremenItem: this.handleDecrementItem,
          removeItem: this.handleRemoveItem,
          clearCart: this.handleClearCart
        }}
      >
        {this.props.children}
      </ProductContext.Provider>
    );
  }
}

export { ProductsConsumer, ProductsProvider };
