import React, { Component, Fragment } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

// load Components
import Navbar from "./components/layouts/Navbar";
import Footer from "./components/layouts/Footer";
import Products from "./components/products/Products";
import ProductDetails from "./components/products/ProductDetails";
import Cart from "./components/Cart/Cart";
import Default from "./components/Default";
import Home from "./components/home/Home";
import CategoryProducts from "./components/products/CategoryProducts";
import DepartmentProducts from "./components/products/DepartmentProducts";
import SearchProducts from "./components/products/SearchProducts";
import Signin from "./components/auth/Signin";
import Signup from "./components/auth/Signup";
import UserUpdate from "./components/auth/UserUpdate";
import PaymentSuccess from "./components/Cart/PaymentSuccess";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Navbar />
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/products" component={Products} exact />
            <Route path="/products/:id" component={ProductDetails} exact />
            <Route path="/category/:id" component={CategoryProducts} exact />
            <Route
              path="/department/:id"
              component={DepartmentProducts}
              exact
            />
            <Route path="/search" component={SearchProducts} exact />
            <Route path="/cart" component={Cart} exact />
            <Route path="/signin" component={Signin} exact />
            <Route path="/signup" component={Signup} exact />
            <Route path="/user/edit/:id" component={UserUpdate} exact />
            <Route path="/success-payment" component={PaymentSuccess} exact />
            <Route component={Default} />
          </Switch>
          <Footer />
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
