import React, { Component } from "react";
import { Link } from "react-router-dom";
// import PaypalPayment from "./PaypalPayment";
import styled from "styled-components";
import StripePayment from "./StripePayment";
import AuthHelperMethods from "../../helpers/AuthHelper";

export default class CartTotal extends Component {
  auth = new AuthHelperMethods();

  render() {
    const { cartTotal, clearCart } = this.props.products;

    return (
      <CartTotalWrapper>
        <div className="row">
          <div className="col-md-2">&nbsp;</div>
          <div className="col-md-8 cart-total-wrapper">
            <div className="pull-right">
              <div className="total-price">
                <span className="text-title"> Total: </span>
                <strong>${cartTotal} </strong>
              </div>
              <Link to="/cart">
                <button
                  className="btn btn-outline-danger text-uppercase mb-3 px-5"
                  type="button"
                  onClick={() => {
                    clearCart();
                  }}
                >
                  clear cart
                </button>
              </Link>
            </div>

            <div className="checkout-btn-area">
              <Link to="/products">
                <button
                  className="btn btn-outline-danger text-uppercase mb-3 px-5 continue-btn"
                  type="button"
                >
                  Continue Shopping
                </button>
              </Link>
              <br />
              {!this.auth.loggedIn() && (
                <Link to="/signin">
                  <button
                    className="btn btn-outline-danger text-uppercase mb-3 px-5 checkout-btn"
                    type="button"
                  >
                    Signin for Checkout
                  </button>
                </Link>
              )}
              {this.auth.loggedIn() && (
                <StripePayment
                  name={"Please give your card info!"}
                  description={"And thanks for shop from us!"}
                  amount={cartTotal}
                />
              )}
            </div>
          </div>
        </div>
      </CartTotalWrapper>
    );
  }
}

const CartTotalWrapper = styled.div`
  .cart-total-wrapper {
    background: #fff;
    padding: 20px;

    .total-price {
      font-size: 26px;
      font-family: "Playfair Display", serif;
      font-style: italic;
      padding: 10px;
    }
    .continue-btn {
      background: #ea7272;
      border-color: #ea7272;
      color: #fff;
    }
    .checkout-btn {
      background: blue;
      border-color: blue;
      color: #fff;
    }
  }
`;
