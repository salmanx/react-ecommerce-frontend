import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import withAuth from "../withAuth";

class PaymentSuccess extends React.Component {
  render() {
    const backgroundColor = {
      background: "#f8f9fa"
    };
    return (
      <PaymentSuccessWrapper>
        <div className="py-5" style={backgroundColor}>
          <div className="container">
            <div className="col-md-8 empty-cart-content">
              <h2>Thanks for shopping from us! </h2>
              <h4>We will knock your door soon!</h4>
              <p>
                <Link
                  to="/products"
                  className="btn btn-sm btn-primary shop-btn"
                >
                  Shop Now !
                </Link>
              </p>
            </div>
          </div>
        </div>
      </PaymentSuccessWrapper>
    );
  }
}

export default withAuth(PaymentSuccess);

const PaymentSuccessWrapper = styled.div`
  .empty-cart-content {
    h2,
    h4 {
      text-transform: uppercase;
      color: #ea7272;
      font-family: "Lato", sans-serif;
      padding: 20px 0px;
      font-weight: bold;
      letter-spacing: 2px;
      line-height: 50px;
    }

    .shop-btn {
      font-style: italic;
      font-family: "Playfair Display", serif;
      margin: 20px 0px;
      background: #ea7272;
      border-color: #ea7272;
      font-size: 30px;
      width: 240px;
    }
  }
`;
