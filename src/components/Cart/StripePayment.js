import React from "react";
import axios from "axios";
import StripeCheckout from "react-stripe-checkout";
import { apiUrl } from "../../config";
import STRIPE_PUBLISHABLE from "../../constants/stripe";
import AuthHelperMethods from "../../helpers/AuthHelper";

const auth = new AuthHelperMethods();
const CURRENCY = "USD";
const fromEuroToCent = amount => amount * 100;

const successPayment = data => {
  alert("Payment Successful");
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };
  headers["Authorization"] = auth.gettoken();
  const orders = { orders: JSON.parse(localStorage.carts) };
  axios
    .post(`${apiUrl}/order`, orders, { headers })
    .then(response => {
      console.log(response);
    })
    .catch(error => {
      console.log(error);
    });
  localStorage.removeItem("carts");
  window.location.href = "/success-payment";
};
const errorPayment = data => {
  alert("Payment Error");
};
const onToken = (amount, description) => token =>
  axios
    .post(`${apiUrl}/payment`, {
      description,
      source: token.id,
      currency: CURRENCY,
      amount: fromEuroToCent(amount)
    })
    .then(successPayment)
    .catch(errorPayment);

class StripePayment extends React.Component {
  render() {
    const { name, description, amount } = this.props;
    return (
      <StripeCheckout
        name={name}
        description={description}
        amount={fromEuroToCent(amount)}
        token={onToken(amount, description)}
        currency={CURRENCY}
        stripeKey={STRIPE_PUBLISHABLE}
      />
    );
  }
}

export default StripePayment;
