import React from "react";
import { Link } from "react-router-dom";

export default function CartItem({ cartProduct, products }) {
  const { product_id, name, image, price, total, count } = cartProduct;
  const { incrementItem, decremenItem, removeItem } = products;

  return (
    <tr className="cart-item">
      <td className="col-md-2 name-col">
        <Link to={`/products/${product_id}`}>
          <img src={"assets/product_images/" + image} alt={name} />
          <h6>{name}</h6>
        </Link>
      </td>
      <td className="col-md-2">
        <h6>
          <strong>${price}</strong>
        </h6>
      </td>
      <td className="col-md-2">
        <div>
          <span
            className="btn btn-black mx-1 qty-btn"
            onClick={() => {
              return decremenItem(product_id);
            }}
          >
            -
          </span>
          <span className="btn btn-black mx-1 count-width">{count}</span>
          <span
            className="btn btn-black mx-1 qty-btn"
            onClick={() => {
              return incrementItem(product_id);
            }}
          >
            +
          </span>
        </div>
      </td>

      <td className="col-md-2">
        <strong> ${total} </strong>
      </td>
      <td className="col-md-2">
        <div onClick={() => removeItem(product_id)}>
          <i className="fa fa-trash" />
        </div>
      </td>
    </tr>
  );
}
