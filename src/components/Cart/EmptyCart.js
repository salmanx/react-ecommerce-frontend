import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

export default function EmptyCart() {
  const backgroundColor = {
    background: "#f8f9fa"
  };
  return (
    <EmptyCartWrapper>
      <div className="py-5" style={backgroundColor}>
        <div className="container">
          <div className="col-md-8 empty-cart-content">
            <h2>Unfortunately you have no products in cart! </h2>
            <p>
              <Link to="/products" className="btn btn-sm btn-primary shop-btn">
                Shop Now !
              </Link>
            </p>
          </div>
        </div>
      </div>
    </EmptyCartWrapper>
  );
}

const EmptyCartWrapper = styled.div`
  .empty-cart-content {
    h2 {
      text-transform: uppercase;
      color: #ea7272;
      font-family: "Lato", sans-serif;
      padding: 20px 0px;
      font-weight: bold;
      letter-spacing: 2px;
      line-height: 50px;
    }

    .shop-btn {
      font-style: italic;
      font-family: "Playfair Display", serif;
      margin: 20px 0px;
      background: #ea7272;
      border-color: #ea7272;
      font-size: 30px;
      width: 240px;
    }
  }
`;
