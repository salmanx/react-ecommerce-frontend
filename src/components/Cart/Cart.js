import React, { Component } from "react";
import Title from "../Title";
import CartColumns from "./CartColumn";
import EmptyCart from "./EmptyCart";
import CartItems from "./CartItems";
import CartTotal from "./CartTotal";
import { ProductsConsumer } from "../../context";
import styled from "styled-components";

class Cart extends Component {
  render() {
    return (
      <CartWrapper>
        <div className="py-5 cart-wrapper">
          <div className="container">
            <ProductsConsumer>
              {data => {
                if (data.carts.length > 0) {
                  return (
                    <CartWrapper>
                      <Title title="Your cart" />
                      <div className="row">
                        <div className="col-md-2" />
                        <div className="col-md-8">
                          <div className="row">
                            <table className="table table-striped cart-table">
                              <thead>
                                <CartColumns />
                              </thead>
                              <tbody>
                                <CartItems products={data} />
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <CartTotal products={data} />
                    </CartWrapper>
                  );
                } else {
                  return (
                    <div className="row">
                      <EmptyCart />
                    </div>
                  );
                }
              }}
            </ProductsConsumer>
          </div>
        </div>
      </CartWrapper>
    );
  }
}

const CartWrapper = styled.div`
  thead {
    tr {
      background: #fff;
      color: #666666;
      font-family: "Lato", sans-serif;
    }
    th {
      width: 20%;
      text-align: center;
    }
  }
  .cart-wrapper {
    background: #f8f9fa;
  }
  .cart-table {
    .cart-item {
      background: #fff;
      color: #666666;
      .col-md-2 {
        width: 20%;
        text-align: center;
      }
      .name-col {
        font-size: 18px;
        color: #666666;
        img {
          height: 60px;
          width: 60px;
          border-radius: 2px;
        }
        h6 {
          font-size: 14px;
          padding: 10px 0px;
          color: #666666;
        }
        a:hover {
          text-decoration: none;
          color: #666666;
        }
      }
      .fa-trash {
        color: #ea7272;
        cursor: pointer;
      }
    }
  }
  .cart-total {
    background: #fff;
  }
  .count-width {
    width: 10px;
  }

  .qty-btn {
    font-size: 20px;
    font-weight: bold;
    background: #ea7272;
    height: 20px;
    line-height: 8px;
    color: #fff;
  }
`;

export default Cart;
