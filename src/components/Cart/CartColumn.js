import React from "react";

export default function CartColumns() {
  return (
    <tr>
      <th className="col-md-2">Product Name</th>
      <th className="col-md-2">Price</th>
      <th className="col-md-2">Quantity</th>
      <th className="col-md-2">Total</th>
      <th className="col-md-2">Remove</th>
    </tr>
  );
}
