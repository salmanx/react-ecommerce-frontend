import React, { Fragment } from "react";
import CartItem from "./CartItem";

export default function CartItems({ products }) {
  const { carts } = products;
  return (
    <Fragment>
      {carts.map(cart => {
        return (
          <CartItem
            cartProduct={cart}
            products={products}
            key={cart.product_id}
          />
        );
      })}
    </Fragment>
  );
}
