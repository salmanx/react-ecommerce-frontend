import React, { Component } from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

class Pagination extends Component {
  render() {
    const totalPaginator = Math.ceil(this.props.total / 12);
    return (
      <PaginationWrapper>
        <div>
          <nav aria-label="Page navigation">
            <ul className="pagination">
              {this.props.total > 12 &&
                [...Array(totalPaginator).keys()].map(page => (
                  <li key={page + 1}>
                    <NavLink
                      to={`?page=${page + 1}`}
                      activeClassName="active"
                      key={page}
                    >
                      {page + 1}
                    </NavLink>
                  </li>
                ))}
            </ul>
          </nav>
        </div>
      </PaginationWrapper>
    );
  }
}
const PaginationWrapper = styled.div`
  text-align: center;
  .pagination {
    display: inline-block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
  }
  .pagination > li {
    display: inline;
  }
  .pagination > li > a,
  .pagination > li > span {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
  }
  .pagination > li:first-child > a,
  .pagination > li:first-child > span {
    margin-left: 0;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
  }
  .pagination > li:last-child > a,
  .pagination > li:last-child > span {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }
`;

export default Pagination;
