import React, { Component } from "react";
import styled from "styled-components";
import { Redirect } from "react-router";

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.state = {
      keyword: null,
      searchRedirect: false
    };
  }

  onSearchSubmit = e => {
    let keyword = e.target.elements.search.value.trim();
    if (keyword) {
      this.setState(() => ({
        keyword: keyword,
        searchRedirect: true
      }));
    }
  };

  render() {
    if (this.state.searchRedirect) {
      return <Redirect to={`/search?keyword=${this.state.keyword}`} />;
    } else {
      return (
        <SearchWrapper>
          <div className="search">
            <form className="search-form" onSubmit={this.onSearchSubmit}>
              <div className="form-group has-feedback">
                <label className="sr-only">Search</label>
                <input
                  type="text"
                  className="form-control"
                  name="search"
                  id="search"
                  placeholder="search"
                />
                <span className="form-control-feedback">
                  <i className="fa fa-search" />
                </span>
              </div>
            </form>
          </div>
        </SearchWrapper>
      );
    }
  }
}

const SearchWrapper = styled.div`
  .search-form .form-group {
    float: right !important;
    transition: all 0.35s, border-radius 0s;
    width: 32px;
    height: 32px;
    background-color: #fff;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    border-radius: 25px;
    border: 1px solid #ea7272;
    margin: 15px;
  }
  .search-form .form-group input.form-control {
    padding-right: 20px;
    border: 0 none;
    background: transparent;
    box-shadow: none;
    display: block;
  }
  .search-form .form-group input.form-control::-webkit-input-placeholder {
    display: none;
  }
  .search-form .form-group input.form-control:-moz-placeholder {
    /* Firefox 18- */
    display: none;
  }
  .search-form .form-group input.form-control::-moz-placeholder {
    /* Firefox 19+ */
    display: none;
  }
  .search-form .form-group input.form-control:-ms-input-placeholder {
    display: none;
  }
  .search-form .form-group:hover,
  .search-form .form-group.hover {
    width: 100%;
    border-radius: 4px 25px 25px 4px;
  }
  .search-form .form-group span.form-control-feedback {
    position: absolute;
    top: 14px;
    right: 28px;
    z-index: 2;
    display: block;
    width: 35px;
    height: 35px;
    line-height: 34px;
    text-align: center;
    color: #ea7272;
    left: initial;
    font-size: 14px;
  }
`;
