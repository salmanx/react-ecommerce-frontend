import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { ProductsConsumer } from "../../context";

export default class CartNotification extends Component {
  render() {
    return (
      <CartNotificationWrapper>
        <ProductsConsumer>
          {data => {
            return (
              <div className="cart-notification-area">
                <Link to={`/cart`}>
                  <div className="notifications">
                    <i className="fa fa-shopping-cart" />
                    <span className="num">{data.carts.length}</span>
                  </div>
                </Link>
              </div>
            );
          }}
        </ProductsConsumer>
      </CartNotificationWrapper>
    );
  }
}

const CartNotificationWrapper = styled.div`
  .notifications {
    width: 30px;
    height: 30px;
    background: #fff;
    border-radius: 30px;
    box-sizing: border-box;
    text-align: center;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
    margin-top: 15px;
  }

  .notifications:hover .fa {
    color: #ff2c74;
  }
  .notifications .fa {
    color: #ff2c74;
    font-size: 15px;
    top: -3px;
    position: relative;
  }
  .notifications .num {
    position: absolute;
    top: 6px;
    right: 40px;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background: #ea7272;
    color: #fff;
    line-height: 21px;
    font-family: sans-serif;
    text-align: center;
  }
`;
