import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";
import styled from "styled-components";
import Axios from "axios";
import Search from "./Search";
import CartNotification from "./CartNotification";
import { apiUrl } from "../../config";
import AuthHelperMethods from "../../helpers/AuthHelper";

export default class Navbar extends Component {
  auth = new AuthHelperMethods();

  state = {
    categories: []
  };

  componentDidMount() {
    Axios.get(`${apiUrl}/categories`)
      .then(res => {
        this.setState(() => ({ categories: res.data }));
      })
      .catch(err => console.log(err));
  }

  render() {
    return (
      <NavWrapper>
        <div className="app-masthead">
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <nav className="app-nav">
                  <Link to="/" className="app-nav-item navbar-brand">
                    ShopMate
                  </Link>
                </nav>
              </div>
              <div className="col-md-6">
                <nav className="app-nav app-nav-link">
                  {this.state.categories.length > 0 &&
                    this.state.categories.map(cat => (
                      <NavLink
                        to={`/category/${cat.category_id}`}
                        className="app-nav-item"
                        activeClassName="active"
                        key={cat.category_id}
                        onClick={() => {
                          localStorage.setItem("page_title", cat.name);
                        }}
                      >
                        {cat.name}
                      </NavLink>
                    ))}
                </nav>
              </div>
              <div className="col-md-2">
                <Search />
              </div>

              <div className="col-md-1">
                <CartNotification />
              </div>
              <div className="col-md-1">
                <nav className="app-nav app-nav-link">
                  {!this.auth.loggedIn() && (
                    <NavLink
                      to={`/signin`}
                      className="app-nav-item"
                      activeClassName="active"
                    >
                      Login
                    </NavLink>
                  )}

                  {this.auth.loggedIn() && (
                    <NavLink
                      to={`/user/edit/${this.auth.currentUser().customer_id}`}
                      className="app-nav-item"
                      activeClassName="active"
                    >
                      {this.auth.currentUser().name.split(" ")[0]}
                    </NavLink>
                  )}
                </nav>
              </div>
            </div>
          </div>
        </div>
      </NavWrapper>
    );
  }
}

const NavWrapper = styled.div`
  .app-masthead {
    background-color: #fff;
    height: 60px;
    line-height: 40px;
  }
  .app-nav-link {
    text-align: right;
  }
  /* Nav links */
  .app-nav-item {
    position: relative;
    display: inline-block;
    padding: 10px;
    font-weight: 600;
    color: #666666;
    font-size: 16px;
    font-family: "Lato", sans-serif;
    cursor: pointer;
  }
  .app-nav-item:hover,
  .app-nav-item:focus {
    color: #666666;
    text-decoration: none;
  }

  /* Active state gets a caret at the bottom */
  .app-nav .active {
    color: #ea7272;
  }
  .app-nav .active:after {
    position: absolute;
    bottom: 0;
    left: 50%;
    width: 0;
    height: 0;
    margin-left: -5px;
    vertical-align: middle;
    content: " ";
    border-right: 5px solid transparent;
    border-bottom: 5px solid;
    border-left: 5px solid transparent;
  }

  .navbar-brand {
    font-size: 30px;
    color: #ea7272;
    line-height: 40px;
    text-transform: uppercase;
    letter-spacing: 3px;
    margin-left: -50px;

    &:hover {
      color: #ea7272;
    }
  }
`;
