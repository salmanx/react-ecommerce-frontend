import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Axios from "axios";
import { apiUrl } from "../../config";
import AuthHelperMethods from "../../helpers/AuthHelper";

export default class Footer extends Component {
  auth = new AuthHelperMethods();

  constructor(props) {
    super(props);
    this.signout = this.signout.bind(this);
  }

  state = {
    categories: [],
    departments: []
  };

  signout() {
    this.auth.logout();
  }

  componentDidMount() {
    Axios.get(`${apiUrl}/categories`)
      .then(res => {
        this.setState(() => ({ categories: res.data }));
      })
      .catch(err => console.log(err));

    Axios.get(`${apiUrl}/departments`)
      .then(res => {
        this.setState(() => ({ departments: res.data }));
      })
      .catch(err => console.log(err));
  }

  render() {
    return (
      <FooterWrapper>
        <div className="footer">
          <div className="container">
            <div className="row ">
              <div className="col-md-3 ">
                <div className="footer-widget ">
                  <div className="footer-title">Quick Link</div>
                  <ul className="list-unstyled">
                    {!this.auth.loggedIn() && (
                      <div>
                        <li>
                          <Link to={`/signin`}> Login </Link>
                        </li>
                        <li>
                          <Link to={`/signup`}> Register </Link>
                        </li>
                      </div>
                    )}
                    {this.auth.loggedIn() && (
                      <div>
                        <li>
                          <span
                            className="app-nav-item logout-link"
                            onClick={this.signout}
                          >
                            Logout
                          </span>
                        </li>
                      </div>
                    )}
                  </ul>
                </div>
              </div>

              <div className="col-md-3">
                <div className="footer-widget ">
                  <div className="footer-title">Shop by Category</div>
                  <ul className="list-unstyled">
                    {this.state.categories.length > 0 &&
                      this.state.categories.map(cat => (
                        <li key={cat.category_id}>
                          <Link
                            to={`/category/${cat.category_id}`}
                            onClick={() => {
                              localStorage.setItem("page_title", cat.name);
                            }}
                          >
                            {cat.name}
                          </Link>
                        </li>
                      ))}
                  </ul>
                </div>
              </div>

              <div className="col-md-3">
                <div className="footer-widget ">
                  <div className="footer-title">Shop by Department</div>
                  <ul className="list-unstyled">
                    {this.state.departments.length > 0 &&
                      this.state.departments.map(dept => (
                        <li key={dept.department_id}>
                          <Link
                            to={`/department/${dept.department_id}`}
                            onClick={() => {
                              localStorage.setItem("page_title", dept.name);
                            }}
                          >
                            {dept.name}
                          </Link>
                        </li>
                      ))}
                  </ul>
                </div>
              </div>

              <div className="col-md-3">
                <div className="footer-widget ">
                  <div className="footer-title">Social</div>
                  <ul className="list-unstyled">
                    <li>
                      <Link to="/">Twitter</Link>
                    </li>
                    <li>
                      <Link to="/">Google +</Link>
                    </li>
                    <li>
                      <Link to="/">Linkedin</Link>
                    </li>
                    <li>
                      <Link to="/">Facebook</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="row ">
              <div className="col-md-12 text-center">
                <div className="tiny-footer">
                  <p>
                    Copyright © All Rights Reserved 2019 | Develoved by Salman
                    Mahmud
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </FooterWrapper>
    );
  }
}

const FooterWrapper = styled.div`
  .footer {
    background-color: #f4f4f4;
    padding-top: 80px;
    color: #636e74;
    font-weight: 400;
    font-size: 17px;
  }

  .footer-widget {
    padding-bottom: 50px;
  }
  .footer-title {
    font-size: 20px;
    font-weight: 600;
    margin-bottom: 44px;
    color: #ea7272;
  }
  .footer-widget ul li a {
    text-transform: capitalize;
    font-size: 17px;
    color: #636e74;
    display: block;
    font-weight: 600;
  }
  .footer-widget ul li a:hover {
    color: #ea7272;
  }

  .tiny-footer {
    font-size: 13px;
    padding: 14px 0px;
    font-weight: normal;
    background-color: transparent;
    border-top: 1px solid #152e3d;
    color: #888d90;
    line-height: 1;
    font-family: "Playfair Display", serif;
    font-style: italic;
  }
  .logout-link {
    cursor: pointer;
    font-weight: 600;
  }
`;
