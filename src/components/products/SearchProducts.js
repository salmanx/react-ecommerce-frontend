import React, { Component, Fragment } from "react";
import Axios from "axios";
import Title from "../Title";
import Product from "./Product";
import Default from "../Default";

export default class SearchProducts extends Component {
  state = {
    products: []
  };

  componentDidMount() {
    const keyword = this.props.location.search;
    Axios.get(`http://localhost:5000/api/search${keyword}`)
      .then(res => {
        this.setState(() => ({
          products: res.data
        }));
      })
      .catch(err => console.log(err));
  }

  render() {
    const backgroundColor = {
      background: "#f8f9fa"
    };

    if (this.state.products.length < 1) {
      return <Default />;
    } else {
      return (
        <Fragment>
          <div className="py-5" style={backgroundColor}>
            <div className="container">
              <Title title="Your Search" />
              <div className="row">
                {this.state.products.length > 0 &&
                  this.state.products.map(product => (
                    <Product product={product} key={product.product_id} />
                  ))}
              </div>
            </div>
          </div>
        </Fragment>
      );
    }
  }
}
