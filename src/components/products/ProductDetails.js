import React, { Component } from "react";
import { ProductsConsumer } from "../../context";
import styled from "styled-components";
import Axios from "axios";

export default class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {}
    };
  }
  alreadyInCart = id => {
    if (localStorage.carts) {
      const prodctsInStorage = JSON.parse(localStorage.carts);
      const item = prodctsInStorage.find(p => p.product_id === id);
      if (item) return true;
      else return false;
    }
  };

  componentDidMount() {
    Axios.get(
      `http://localhost:5000/api/products/${this.props.match.params.id}`
    )
      .then(res => {
        this.setState(() => ({ product: res.data }));
      })
      .catch(err => console.log(err));
  }

  render() {
    return (
      <ProductsConsumer>
        {data => {
          const {
            product_id,
            name,
            image,
            price,
            discounted_price,
            description
          } = this.state.product;
          return (
            <ProductDetailsWrapper>
              <div className="container">
                <div className="row">
                  <div className="single-item">
                    <div className="left-set col-md-6">
                      <img src={"/assets/product_images/" + image} alt={name} />
                    </div>
                    <div className="right-set col-md-6">
                      <div className="name">{name}</div>
                      <div className="price">${price}</div>
                      <div className="subname">
                        Discount ${discounted_price}
                      </div>
                      <div className="description">
                        <p>{description}</p>
                      </div>

                      {this.alreadyInCart(product_id) && (
                        <button
                          onClick={e => {
                            e.preventDefault();
                            data.removeItem(product_id);
                          }}
                        >
                          Remove From Cart
                        </button>
                      )}

                      {!this.alreadyInCart(product_id) && (
                        <button
                          onClick={e => {
                            e.preventDefault();
                            data.handleAddToCart(this.state.product);
                          }}
                        >
                          Add to cart
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </ProductDetailsWrapper>
          );
        }}
      </ProductsConsumer>
    );
  }
}

const ProductDetailsWrapper = styled.div`
  .single-item {
    border: solid 6px #fff;
    border-radius: 10px;
    background: #f5f5f5;
    padding: 50px;
    width: 100%;
  }

  .left-set,
  .right-set {
    position: relative;
    height: 100%;
  }
  .left-set {
    float: left;
    overflow: hidden;
    img {
    }
  }
  .right-set {
    float: right;
    padding: 2rem 1rem;
    box-sizing: border-box;
    .name,
    .subname {
      font-weight: bold;
    }
    .name {
      font-size: 2rem;
      line-height: 2rem;
    }
    .subname {
      font-size: 0.8rem;
      line-height: 1rem;
    }
    .price {
      padding: 1rem 0;
      font-size: 2rem;
      font-weight: bold;
    }
    .description p {
      font-size: 0.9rem;
      text-align: justify;
      hyphens: auto;
    }

    button {
      width: 100%;
      height: 2.5rem;
      cursor: pointer;
      background: #ea7272;
      border: solid 2px #ea7272;
      border-radius: 4px;
      font-weight: bold;
      letter-spacing: 1px;
      transition: all 0.4s ease;
      color: #fff;
      font-family: "Lato", sans-serif;
      text-transform: uppercase;
      &:hover {
        color: #fff;
        background: #ea7272;
      }
    }
  }
`;
