import React, { Component, Fragment } from "react";
import Axios from "axios";
import Title from "../Title";
import Product from "./Product";
import Pagination from "../layouts/Pagination";
import { apiUrl } from "../../config";

export default class DepartmentProducts extends Component {
  state = {
    products: [],
    total: null
  };

  // Note:
  // Unfortunately I need to use both componentDidMount() and componentWillReceiveProps()
  // to fetch same data. Application does not behave properly.

  componentDidMount() {
    const page = this.props.location.search;
    Axios.get(`${apiUrl}/departments/${this.props.match.params.id}${page}`)
      .then(res => {
        this.setState(() => ({
          products: res.data.products,
          total: res.data.count
        }));
      })
      .catch(err => console.log(err));
  }

  componentWillReceiveProps(newProps) {
    const page = newProps.location.search;

    Axios.get(`${apiUrl}/departments/${newProps.match.params.id}${page}`)
      .then(res => {
        this.setState(() => ({
          products: res.data.products,
          total: res.data.count
        }));
      })
      .catch(err => console.log(err));
  }

  render() {
    const backgroundColor = {
      background: "#f8f9fa"
    };
    const pageTitle = localStorage.getItem("page_title")
      ? localStorage.getItem("page_title")
      : "Our Products";

    return (
      <Fragment>
        <div className="py-5" style={backgroundColor}>
          <div className="container">
            <Title title={pageTitle} />
            <div className="row">
              {this.state.products.length > 0 &&
                this.state.products.map(product => (
                  <Product product={product} key={product.product_id} />
                ))}
            </div>
            <Pagination total={this.state.total} />
          </div>
        </div>
      </Fragment>
    );
  }
}
