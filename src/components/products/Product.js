import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { ProductsConsumer } from "../../context";

export default class Product extends Component {
  alreadyInCart = id => {
    if (localStorage.carts) {
      const prodctsInStorage = JSON.parse(localStorage.carts);
      const item = prodctsInStorage.find(p => p.product_id === id);
      if (item) return true;
      else return false;
    }
  };

  render() {
    const { product_id, name, image, image_2, price } = this.props.product;
    return (
      <ProductWrapper className="col-md-3">
        <div className="product-card">
          <ProductsConsumer>
            {data => (
              <Fragment>
                <div className="product-image">
                  <Link to={`/products/${product_id}`}>
                    <img
                      className="pic-1"
                      alt={name}
                      src={"/assets/product_images/" + image}
                    />
                    <img
                      className="pic-2"
                      alt={name}
                      src={"/assets/product_images/" + image_2}
                    />
                  </Link>
                  <ul className="social">
                    <li>
                      <Link to={`/products/${product_id}`} data-tip="Full view">
                        <i className="fa fa-search" />
                      </Link>
                    </li>
                    <li>
                      <a
                        href="#"
                        data-tip="Add to Cart"
                        onClick={e => {
                          e.preventDefault();
                          data.handleAddToCart(this.props.product);
                        }}
                      >
                        <i className="fa fa-shopping-cart" />
                      </a>
                    </li>
                  </ul>
                  {this.alreadyInCart(product_id) && (
                    <a
                      className="add-to-cart already_in_cart"
                      href="#"
                      onClick={e => {
                        e.preventDefault();
                        data.removeItem(product_id);
                      }}
                    >
                      Remove From Cart
                    </a>
                  )}

                  {!this.alreadyInCart(product_id) && (
                    <a
                      className="add-to-cart"
                      href="#"
                      onClick={e => {
                        e.preventDefault();
                        data.handleAddToCart(this.props.product);
                      }}
                    >
                      Add to cart
                    </a>
                  )}
                </div>
                <div className="product-content">
                  <h3 className="title">
                    <Link to={`/products/${product_id}`}>{name}</Link>
                  </h3>
                  <span className="price">${price}</span>
                </div>
              </Fragment>
            )}
          </ProductsConsumer>
        </div>
      </ProductWrapper>
    );
  }
}

Product.propTypes = {
  product: PropTypes.shape({
    product_id: PropTypes.number,
    name: PropTypes.string,
    img: PropTypes.string,
    price: PropTypes.number,
    inCart: PropTypes.bool
  })
};

const ProductWrapper = styled.div`
  .product-card {
    font-family: "Open Sans", sans-serif;
    position: relative;
  }
  .product-card .product-image {
    overflow: hidden;
    position: relative;
  }
  .product-card .product-image a {
    display: block;
  }
  .product-card .product-image img {
    width: 100%;
    height: 280px;
  }
  .product-image .pic-1 {
    opacity: 1;
    transition: all 0.5s;
  }
  .product-card:hover .product-image .pic-1 {
    opacity: 0;
  }
  .product-image .pic-2 {
    width: 100%;
    height: 100%;
    opacity: 0;
    position: absolute;
    top: 0;
    left: 0;
    transition: all 0.5s;
  }
  .product-card:hover .product-image .pic-2 {
    opacity: 1;
  }
  .product-card .social {
    padding: 0;
    margin: 0;
    position: absolute;
    bottom: 50px;
    right: 25px;
    z-index: 1;
  }
  .product-card .social li {
    margin: 0 0 10px;
    display: block;
    transform: translateX(100px);
    transition: all 0.5s;
  }
  .product-card:hover .social li {
    transform: translateX(0);
  }
  .product-card:hover .social li:nth-child(2) {
    transition-delay: 0.15s;
  }
  .product-card:hover .social li:nth-child(3) {
    transition-delay: 0.25s;
  }
  .product-card .social li a {
    color: #ea7272;
    background-color: #fff;
    font-size: 17px;
    line-height: 45px;
    text-align: center;
    height: 45px;
    width: 45px;
    border-radius: 50%;
    display: block;
    transition: all 0.3s ease 0s;
  }
  .product-card .social li a:hover {
    color: #fff;
    background-color: #ea7272;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
  }
  .product-card .social li a:after,
  .product-card .social li a:before {
    content: attr(data-tip);
    color: #fff;
    background-color: #000;
    font-size: 12px;
    line-height: 22px;
    border-radius: 3px;
    padding: 0 5px;
    white-space: nowrap;
    opacity: 0;
    transform: translateX(-50%);
    position: absolute;
    left: 50%;
    top: -30px;
  }
  .product-card .social li a:after {
    content: "";
    height: 15px;
    width: 15px;
    border-radius: 0;
    transform: translateX(-50%) rotate(45deg);
    top: -22px;
    z-index: -1;
  }
  .product-card .social li a:hover:after,
  .product-card .social li a:hover:before {
    opacity: 1;
  }
  .product-card .add-to-cart {
    color: #fff;
    background-color: #404040;
    font-size: 15px;
    text-align: center;
    width: 100%;
    padding: 10px 0;
    display: block;
    position: relative;
    left: 0;
    bottom: -100%;
    transition: all 0.3s;
  }
  .product-card .add-to-cart:hover {
    background-color: #ea7272;
    text-decoration: none;
  }
  .product-card:hover .add-to-cart {
    bottom: 0;
  }
  .product-card .product-new-label {
    background-color: #ea7272;
    color: #fff;
    font-size: 17px;
    padding: 5px 10px;
    position: absolute;
    right: 0;
    top: 0;
    transition: all 0.3s;
  }
  .product-card:hover .product-new-label {
    opacity: 0;
  }
  .product-card .product-content {
    padding: 20px 10px;
    text-align: center;
  }
  .product-card .title {
    font-size: 17px;
    margin: 0 0 7px;
  }
  .product-card .title a {
    color: #303030;
  }
  .product-card .title a:hover {
    color: #ea7272;
  }
  .product-card .price {
    color: #303030;
    font-size: 15px;
  }

  .already_in_cart {
    background: #ea7272 !important;
  }
  @media screen and (max-width: 990px) {
    .product-card {
      margin-bottom: 30px;
    }
  }
`;
