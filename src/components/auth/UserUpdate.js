import React from "react";
import styled from "styled-components";
import Axios from "axios";
import { apiUrl } from "../../config";
import withAuth from "../withAuth";
import AuthHelperMethods from "../../helpers/AuthHelper";

class UserUpdate extends React.Component {
  auth = new AuthHelperMethods();

  constructor(props) {
    super(props);
    this.handleUserUpdate = this.handleUserUpdate.bind(this);

    this.state = {
      email: "",
      password: "",
      name: "",
      error: "",
      user: {}
    };
  }

  componentDidMount() {
    if (this.auth.currentUser()) {
      const headers = {
        Accept: "application/json",
        "Content-Type": "application/json"
      };
      // Setting Authorization header
      if (this.auth.loggedIn()) {
        headers["Authorization"] = this.auth.gettoken();
      }

      Axios.get(`${apiUrl}/customer/${this.auth.currentUser().customer_id}`, {
        headers
      })
        .then(res => {
          this.setState(() => ({
            user: res.data
          }));
        })
        .catch(err => console.log(err));
    }
  }

  handleUserUpdate(e) {
    e.preventDefault();

    if (!e.target.elements.name.value) {
      this.setState({ error: "Please provide your valid name" });
    } else if (!e.target.elements.password.value) {
      this.setState({ error: "Please provide your valid password" });
    } else {
      const name = e.target.elements.name.value;
      const password = e.target.elements.password.value;
      const credit_card = e.target.elements.credit_card.value;
      const postal_code = e.target.elements.postal_code.value;
      const address_1 = e.target.elements.address_1.value;
      const address_2 = e.target.elements.address_2.value;
      const city = e.target.elements.city.value;
      const region = e.target.elements.region.value;
      const country = e.target.elements.country.value;
      const day_phone = e.target.elements.day_phone.value;
      const mob_phone = e.target.elements.mob_phone.value;
      const headers = {
        Accept: "application/json",
        "Content-Type": "application/json"
      };
      // Setting Authorization header
      if (this.auth.loggedIn()) {
        headers["Authorization"] = this.auth.gettoken();
      }

      Axios.put(
        `${apiUrl}/customer/${this.auth.currentUser().customer_id}`,
        {
          name,
          password,
          credit_card,
          postal_code,
          address_1,
          address_2,
          region,
          country,
          city,
          day_phone,
          mob_phone
        },
        { headers }
      )
        .then(response => {
          localStorage.removeItem("token");
          this.props.history.push(`/signin`);
        })
        .catch(error => {
          console.log(error);
          this.setState(() => {
            return {
              error: error
            };
          });
        });
    }
  }

  render() {
    return (
      <UserUpdateWrapper>
        <div id="login">
          <h3 className="text-center text-white pt-5 page-title">
            You can update your information here
          </h3>
          <div className="container">
            <div
              id="login-row"
              className="row justify-content-center align-items-center"
            >
              <div id="login-column" className="col-md-6">
                <div id="login-box" className="col-md-12">
                  <form
                    id="login-form"
                    className="form"
                    onSubmit={this.handleUserUpdate}
                  >
                    {this.state.error && (
                      <p className="alert alert-danger">{this.state.error}</p>
                    )}
                    <div className="form-group">
                      <label className="text-info">Name:</label>
                      <br />
                      <input
                        type="text"
                        name="name"
                        id="name"
                        defaultValue={this.state.user.name || ""}
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <label className="text-info">Password:</label>
                      <br />
                      <input
                        type="password"
                        name="password"
                        id="password"
                        className="form-control"
                      />
                    </div>

                    <div className="form-group row">
                      <div className="col-md-6">
                        <label className="text-info">Post Code:</label>
                        <br />
                        <input
                          type="text"
                          name="postal_code"
                          id="postal_code"
                          defaultValue={this.state.user.postal_code || ""}
                          className="form-control"
                        />
                      </div>
                      <div className="col-md-6">
                        <label className="text-info">Credit card: </label>
                        <br />
                        <input
                          type="text"
                          name="credit_card"
                          id="credit_card"
                          defaultValue={this.state.user.credit_card || ""}
                          className="form-control"
                        />
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-md-6">
                        <label className="text-info">Address: 1</label>
                        <br />
                        <input
                          type="text"
                          name="address_1"
                          id="address"
                          defaultValue={this.state.user.address_1 || ""}
                          className="form-control"
                        />
                      </div>

                      <div className="col-md-6">
                        <label className="text-info">Address 2:</label>
                        <br />
                        <input
                          type="text"
                          name="address_2"
                          id="address_2"
                          defaultValue={this.state.user.address_2 || ""}
                          className="form-control"
                        />
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-md-4">
                        <label className="text-info">City:</label>
                        <br />
                        <input
                          type="text"
                          name="city"
                          id="city"
                          defaultValue={this.state.user.city || ""}
                          className="form-control"
                        />
                      </div>
                      <div className="col-md-4">
                        <label className="text-info">Region: </label>
                        <br />
                        <input
                          type="text"
                          name="region"
                          id="region"
                          defaultValue={this.state.user.region || ""}
                          className="form-control"
                        />
                      </div>
                      <div className="col-md-4">
                        <label className="text-info">Country: </label>
                        <br />
                        <input
                          type="text"
                          name="country"
                          id="country"
                          defaultValue={this.state.user.country || ""}
                          className="form-control"
                        />
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-md-6">
                        <label className="text-info">Telephone:</label>
                        <br />
                        <input
                          type="text"
                          name="day_phone"
                          id="phone"
                          defaultValue={this.state.user.day_phone || ""}
                          className="form-control"
                        />
                      </div>
                      <div className="col-md-6">
                        <label className="text-info">Mobile: </label>
                        <br />
                        <input
                          type="text"
                          name="mob_phone"
                          id="mobile"
                          defaultValue={this.state.user.mob_phone || ""}
                          className="form-control"
                        />
                      </div>
                    </div>

                    <div className="form-group">
                      <br />
                      <input
                        type="submit"
                        name="submit"
                        className="btn btn-info btn-md"
                        value="submit"
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </UserUpdateWrapper>
    );
  }
}

const UserUpdateWrapper = styled.div`
  background: #f8f9fa;
  padding: 50px 0px;
  #login .container #login-row #login-column #login-box {
    margin: 20px;
    max-width: 600px;
    min-height: 650px;
    border: 1px solid #fff;
    background-color: #fff;
  }
  #login .container #login-row #login-column #login-box #login-form {
    padding: 40px 20px 20px 20px;
  }
  #login
    .container
    #login-row
    #login-column
    #login-box
    #login-form
    #register-link {
    margin-top: -85px;
  }
  .page-title {
    text-transform: uppercase;
    color: #ea7272 !important;
    font-family: "Playfair Display", serif;
    padding: 20px;
    font-style: italic;
    font-weight: bold;
  }
`;

export default withAuth(UserUpdate);
