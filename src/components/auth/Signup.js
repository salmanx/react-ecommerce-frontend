import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Axios from "axios";
import { apiUrl } from "../../config";
import AuthHelperMethods from "../../helpers/AuthHelper";

export default class Signup extends React.Component {
  auth = new AuthHelperMethods();

  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);

    this.state = {
      email: "",
      password: "",
      name: "",
      error: ""
    };
  }

  handleRegister(e) {
    e.preventDefault();

    if (!e.target.elements.name.value) {
      this.setState({ error: "Please provide your valid name" });
    } else if (!e.target.elements.email.value) {
      this.setState({ error: "Please provide your valid email" });
    } else if (!e.target.elements.password.value) {
      this.setState({ error: "Please provide your valid password" });
    } else {
      const email = e.target.elements.email.value;
      const password = e.target.elements.password.value;
      const name = e.target.elements.name.value;

      Axios.post(`${apiUrl}/signup`, {
        email,
        password,
        name
      })
        .then(response => {
          console.log(response);
          localStorage.setItem("token", response.data.token);
          const user = this.auth.currentUser();
          this.props.history.push(`/user/edit/${user.customer_id}`);
        })
        .catch(error => {
          console.log(error.response.data);
          this.setState(() => {
            return {
              error: error.response.data
            };
          });
        });
    }
  }

  render() {
    return (
      <SignupWrapper>
        <div id="login">
          <h3 className="text-center text-white pt-5 page-title">
            Please register here
          </h3>
          <div className="container">
            <div
              id="login-row"
              className="row justify-content-center align-items-center"
            >
              <div id="login-column" className="col-md-6">
                <div id="login-box" className="col-md-12">
                  <form
                    id="login-form"
                    className="form"
                    onSubmit={this.handleRegister}
                  >
                    {this.state.error && (
                      <p className="alert alert-danger">{this.state.error}</p>
                    )}
                    <div className="form-group">
                      <label className="text-info">Name:</label>
                      <br />
                      <input
                        type="text"
                        name="name"
                        id="name"
                        className="form-control"
                      />
                    </div>

                    <div className="form-group">
                      <label className="text-info">Email:</label>
                      <br />
                      <input
                        type="text"
                        name="email"
                        id="email"
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <label className="text-info">Password:</label>
                      <br />
                      <input
                        type="password"
                        name="password"
                        id="password"
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <br />
                      <input
                        type="submit"
                        name="submit"
                        className="btn btn-info btn-md"
                        value="submit"
                      />
                    </div>
                    <div id="register-link" className="text-right">
                      <Link to={`/signin`} className="text-info">
                        Lgin here
                      </Link>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </SignupWrapper>
    );
  }
}

const SignupWrapper = styled.div`
  background: #f8f9fa;
  padding: 50px 0px;
  #login .container #login-row #login-column #login-box {
    margin: 20px;
    max-width: 600px;
    height: 460px;
    border: 1px solid #fff;
    background-color: #fff;
  }
  #login .container #login-row #login-column #login-box #login-form {
    padding: 40px 20px 20px 20px;
  }
  #login
    .container
    #login-row
    #login-column
    #login-box
    #login-form
    #register-link {
    margin-top: -85px;
  }
  .page-title {
    text-transform: uppercase;
    color: #ea7272 !important;
    font-family: "Playfair Display", serif;
    padding: 20px;
    font-style: italic;
    font-weight: bold;
  }
`;
