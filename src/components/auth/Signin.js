import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Axios from "axios";
import { apiUrl } from "../../config";

export default class Signin extends React.Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);

    this.state = {
      email: "",
      password: "",
      error: ""
    };
  }

  handleLogin(e) {
    e.preventDefault();

    if (!e.target.elements.email.value || !e.target.elements.password.value) {
      this.setState({ error: "Please provide your valid credentials" });
    } else {
      const email = e.target.elements.email.value;
      const password = e.target.elements.password.value;
      Axios.post(`${apiUrl}/signin`, {
        email,
        password
      })
        .then(response => {
          // console.log(response);
          localStorage.setItem("token", response.data.token);
          this.props.history.push("/products");
        })
        .catch(error => {
          console.log(error.response.data);
          this.setState(() => {
            return {
              error: error.response.data
            };
          });
        });
    }
  }

  render() {
    return (
      <SigninWrapper>
        <div id="login">
          <h3 className="text-center text-white pt-5 page-title">
            Please signin here
          </h3>
          <div className="container">
            <div
              id="login-row"
              className="row justify-content-center align-items-center"
            >
              <div id="login-column" className="col-md-6">
                <div id="login-box" className="col-md-12">
                  <form
                    id="login-form"
                    className="form"
                    onSubmit={this.handleLogin}
                  >
                    {this.state.error && (
                      <p className="alert alert-danger">{this.state.error}</p>
                    )}

                    <div className="form-group">
                      <label className="text-info">Email:</label>
                      <br />
                      <input
                        type="text"
                        name="email"
                        id="email"
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <label className="text-info">Password:</label>
                      <br />
                      <input
                        type="text"
                        name="password"
                        id="password"
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <br />
                      <input
                        type="submit"
                        name="submit"
                        className="btn btn-info btn-md"
                        value="submit"
                      />
                    </div>
                    <div id="register-link" className="text-right">
                      <Link to={`/signup`} className="text-info">
                        Register here
                      </Link>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </SigninWrapper>
    );
  }
}

const SigninWrapper = styled.div`
  background: #f8f9fa;
  padding: 50px 0px;
  #login .container #login-row #login-column #login-box {
    margin: 20px;
    max-width: 600px;
    height: 320px;
    border: 1px solid #fff;
    background-color: #fff;
  }
  #login .container #login-row #login-column #login-box #login-form {
    padding: 40px 20px 20px 20px;
  }
  #login
    .container
    #login-row
    #login-column
    #login-box
    #login-form
    #register-link {
    margin-top: -85px;
  }
  .page-title {
    text-transform: uppercase;
    color: #ea7272 !important;
    font-family: "Playfair Display", serif;
    padding: 20px;
    font-style: italic;
    font-weight: bold;
  }
`;
