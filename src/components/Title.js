import React from "react";
import styled from "styled-components";

export default function Title({ title }) {
  return (
    <TitleWrapper>
      <div className="row">
        <div className="col-10 mx-auto my-2 text-center text-title">
          <h1 className="title-heading">
            <strong>{title}</strong>
          </h1>
        </div>
      </div>
    </TitleWrapper>
  );
}

const TitleWrapper = styled.div`
  .title-heading {
    text-transform: uppercase;
    color: #ea7272;
    font-family: "Playfair Display", serif;
    padding: 20px;
    font-style: italic;
  }
`;
