import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Advertise from "./home/Advertise";

class Default extends Component {
  render() {
    const backgroundColor = {
      background: "#f8f9fa"
    };
    return (
      <DefaultWrapper>
        <div className="py-5" style={backgroundColor}>
          <div className="container">
            <div className="col-md-8 default-cp-content">
              <h2>
                <strong>OPS!!</strong> The page or product you are looking for
                is not found.{" "}
              </h2>
              <p>
                <Link
                  to="/products"
                  className="btn btn-sm btn-primary shop-btn"
                >
                  Shop Now !
                </Link>
              </p>
            </div>
          </div>
        </div>
        <Advertise />
      </DefaultWrapper>
    );
  }
}

const DefaultWrapper = styled.div`
  .default-cp-content {
    h2 {
      text-transform: uppercase;
      color: #ea7272;
      font-family: "Lato", sans-serif;
      padding: 20px 0px;
      font-weight: bold;
      letter-spacing: 2px;
      line-height: 50px;
    }

    .shop-btn {
      font-style: italic;
      font-family: "Playfair Display", serif;
      margin: 20px 0px;
      background: #ea7272;
      border-color: #ea7272;
      font-size: 30px;
      width: 240px;
    }
  }
`;

export default Default;
