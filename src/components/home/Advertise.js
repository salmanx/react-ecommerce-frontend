import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

function Advertise(props) {
  return (
    <AdvertiseWrapper>
      <div className="advertise-area">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-12 col-lg-7 mb-5">
              <a href="#">
                <img
                  src="assets/img//blog_1.jpg"
                  alt="Image placeholder"
                  className="img-fluid rounded"
                />
              </a>
            </div>
            <div className="col-md-12 col-lg-5 text-center pl-md-5 advertise-content">
              <h2>Always on discount!</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Quisquam iste dolor accusantium facere corporis ipsum animi
                deleniti fugiat. Ex, veniam?
              </p>
              <p>
                <Link to="/signup" className="btn btn-primary btn-sm reg-btn">
                  Let's Register!
                </Link>
              </p>
            </div>
          </div>
        </div>
      </div>
    </AdvertiseWrapper>
  );
}

const AdvertiseWrapper = styled.div`
  .advertise-area {
    padding: 50px;
  }
  .advertise-content {
    h2 {
      color: #ea7272;
      font-size: 40px;
      font-family: "Playfair Display", serif;
    }
    .reg-btn {
      background: #ea7272;
      border-color: #ea7272;
      font-size: 30px;
      width: 240px;
      font-family: "Playfair Display", serif;
      font-style: italic;
    }
  }
`;

export default Advertise;
