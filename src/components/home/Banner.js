import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

function Banner(props) {
  return (
    <BannerWrapper>
      <div className="site-blocks-cover">
        <div className="container">
          <div className="row">
            <div className="col-md-8 banner-text">
              <h1 className="mb-2">Finding Your Perfect TShirts</h1>
              <div className="intro-text">
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Phasellus at iaculis quam. Integer accumsan tincidunt
                  fringilla.
                </p>
                <p>
                  <Link
                    to="/products"
                    className="btn btn-sm btn-primary shop-btn"
                  >
                    Shop Now
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </BannerWrapper>
  );
}

const BannerWrapper = styled.div`
  .site-blocks-cover {
    background: url(/assets/img/hero_1.jpg);
    h1 {
      font-size: 45px;
      font-weight: bold;
      color: #ea7272;
      font-family: "Playfair Display", serif;
    }
    p {
      font-size: 20px;
      line-height: 30px;
      font-style: italic;
      font-family: "Playfair Display", serif;
    }
    .intro-text {
      font-size: 16px;
      line-height: 1.5;
    }
    .banner-text {
      padding: 130px 50px;
    }

    .shop-btn {
      background: #ea7272;
      border-color: #ea7272;
      font-size: 30px;
      width: 240px;
    }
  }
`;

export default Banner;
