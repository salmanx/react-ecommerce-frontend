import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { ProductsConsumer } from "../../context";

export default class Product extends Component {
  alreadyInCart = id => {
    if (localStorage.carts) {
      const prodctsInStorage = JSON.parse(localStorage.carts);
      const item = prodctsInStorage.find(p => p.product_id === id);
      if (item) return true;
      else return false;
    }
  };

  render() {
    const {
      product_id,
      name,
      image,
      image_2,
      price,
      discounted_price
    } = this.props.product;
    return (
      <ProductWrapper className="col-md-3">
        <div className="product-card">
          <ProductsConsumer>
            {data => (
              <Fragment>
                <div className="product-image">
                  <a href="#">
                    <img
                      className="pic-1"
                      src={"/assets/product_images/" + image}
                    />
                    <img
                      className="pic-2"
                      src={"/assets/product_images/" + image_2}
                    />
                  </a>
                  <ul className="social">
                    <li>
                      <Link to={`/products/${product_id}`} data-tip="Full view">
                        <i className="fa fa-search" />
                      </Link>
                    </li>
                    <li>
                      <a
                        href="#"
                        data-tip="Add to Cart"
                        onClick={e => {
                          e.preventDefault();
                          data.handleAddToCart(this.props.product);
                        }}
                      >
                        <i className="fa fa-shopping-cart" />
                      </a>
                    </li>
                  </ul>
                  <span className="product-discount-label">
                    ${discounted_price}
                  </span>
                </div>
                <div className="product-content">
                  <h3 className="title">
                    <Link to={`/products/${product_id}`}>{name}</Link>
                  </h3>
                  <div className="price">${price}</div>
                  {this.alreadyInCart(product_id) && (
                    <a
                      className="add-to-cart already_in_cart"
                      href="#"
                      onClick={e => {
                        e.preventDefault();
                        data.removeItem(product_id);
                      }}
                    >
                      Remove From Cart
                    </a>
                  )}

                  {!this.alreadyInCart(product_id) && (
                    <a
                      className="add-to-cart"
                      href="#"
                      onClick={e => {
                        e.preventDefault();
                        data.handleAddToCart(this.props.product);
                      }}
                    >
                      Add to cart
                    </a>
                  )}
                </div>
              </Fragment>
            )}
          </ProductsConsumer>
        </div>
      </ProductWrapper>
    );
  }
}

Product.propTypes = {
  product: PropTypes.shape({
    product_id: PropTypes.number,
    name: PropTypes.string,
    img: PropTypes.string,
    price: PropTypes.number,
    inCart: PropTypes.bool
  })
};

const ProductWrapper = styled.div`
  .product-card {
    font-family: Raleway, sans-serif;
    text-align: center;
    padding: 0 0 72px;
    border: 1px solid rgba(0, 0, 0, 0.1);
    overflow: hidden;
    position: relative;
    z-index: 1;
    margin: 8px 0px;
  }
  .product-card .product-image {
    position: relative;
    transition: all 0.3s ease 0s;
  }
  .product-card .product-image a {
    display: block;
  }
  .product-card .product-image img {
    width: 100%;
    height: auto;
  }
  .product-card .pic-1 {
    opacity: 1;
    transition: all 0.3s ease-out 0s;
  }
  .product-card:hover .pic-1 {
    opacity: 1;
  }
  .product-card .pic-2 {
    opacity: 0;
    position: absolute;
    top: 0;
    left: 0;
    transition: all 0.3s ease-out 0s;
  }
  .product-card:hover .pic-2 {
    opacity: 1;
  }
  .product-card .social {
    width: 150px;
    padding: 0;
    margin: 0;
    list-style: none;
    opacity: 0;
    transform: translateY(-50%) translateX(-50%);
    position: absolute;
    top: 60%;
    left: 50%;
    z-index: 1;
    transition: all 0.3s ease 0s;
  }
  .product-card:hover .social {
    opacity: 1;
    top: 50%;
  }
  .product-card .social li {
    display: inline-block;
  }
  .product-card .social li a {
    color: #fff;
    background-color: #333;
    font-size: 16px;
    line-height: 40px;
    text-align: center;
    height: 40px;
    width: 40px;
    margin: 0 2px;
    display: block;
    position: relative;
    transition: all 0.3s ease-in-out;
  }
  .product-card .social li a:hover {
    color: #fff;
    background-color: #ef5777;
  }
  .product-card .social li a:after,
  .product-card .social li a:before {
    content: attr(data-tip);
    color: #fff;
    background-color: #000;
    font-size: 12px;
    letter-spacing: 1px;
    line-height: 20px;
    padding: 1px 5px;
    white-space: nowrap;
    opacity: 0;
    transform: translateX(-50%);
    position: absolute;
    left: 50%;
    top: -30px;
  }
  .product-card .social li a:after {
    content: "";
    height: 15px;
    width: 15px;
    border-radius: 0;
    transform: translateX(-50%) rotate(45deg);
    top: -20px;
    z-index: -1;
  }
  .product-card .social li a:hover:after,
  .product-card .social li a:hover:before {
    opacity: 1;
  }
  .product-card .product-discount-label,
  .product-card .product-new-label {
    color: #fff;
    background-color: #ef5777;
    font-size: 12px;
    text-transform: uppercase;
    padding: 2px 7px;
    display: block;
    position: absolute;
    top: 10px;
    left: 0;
  }
  .product-card .product-discount-label {
    background-color: #333;
    left: auto;
    right: 0;
  }
  .product-card .rating {
    color: #ffd200;
    font-size: 12px;
    padding: 12px 0 0;
    margin: 0;
    list-style: none;
    position: relative;
    z-index: -1;
  }
  .product-card .rating li.disable {
    color: rgba(0, 0, 0, 0.2);
  }
  .product-card .product-content {
    background-color: #fff;
    text-align: center;
    padding: 12px 0;
    margin: 0 auto;
    position: absolute;
    left: 0;
    right: 0;
    bottom: -27px;
    z-index: 1;
    transition: all 0.3s;
  }
  .product-card:hover .product-content {
    bottom: 0;
  }
  .product-card .title {
    font-size: 13px;
    font-weight: 400;
    letter-spacing: 0.5px;
    text-transform: capitalize;
    margin: 0 0 10px;
    transition: all 0.3s ease 0s;
  }
  .product-card .title a {
    color: #828282;
  }
  .product-card .title a:hover,
  .product-card:hover .title a {
    color: #ef5777;
  }
  .product-card .price {
    color: #333;
    font-size: 17px;
    font-family: Montserrat, sans-serif;
    font-weight: 700;
    letter-spacing: 0.6px;
    margin-bottom: 8px;
    text-align: center;
    transition: all 0.3s;
  }
  .product-card .price span {
    color: #999;
    font-size: 13px;
    font-weight: 400;
    text-decoration: line-through;
    margin-left: 3px;
    display: inline-block;
  }
  .product-card .add-to-cart {
    color: #000;
    font-size: 13px;
    font-weight: 600;
    text-decoration: none;
    &:hover {
      text-decoration: none;
    }
  }
  @media only screen and (max-width: 990px) {
    .product-card {
      margin-bottom: 30px;
    }
  }
`;
