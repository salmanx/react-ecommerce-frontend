import React, { Component } from "react";
import Banner from "./Banner";
import Advertise from "./Advertise";
import Promo from "./Promo";
import FeatureProducts from "./FeatureProducts";
import styled from "styled-components";

export default class Home extends Component {
  componentDidMount() {
    if (localStorage.page_title) localStorage.removeItem("page_title");
  }
  render() {
    return (
      <HomeWrapper>
        <div>
          <Banner />
          <Promo />
          <FeatureProducts />
          <Advertise />
        </div>
      </HomeWrapper>
    );
  }
}

const HomeWrapper = styled.div``;
