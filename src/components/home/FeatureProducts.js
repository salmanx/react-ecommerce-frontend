import React, { Component } from "react";
import Axios from "axios";
import styled from "styled-components";
import Title from "../Title";
import FeatureProduct from "./FeatureProduct";
import { apiUrl } from "../../config";

export default class FeatureProducts extends Component {
  state = {
    products: []
  };

  componentDidMount() {
    Axios.get(`${apiUrl}/products`)
      .then(res => {
        this.setState(() => ({ products: res.data.products }));
      })
      .catch(err => console.log(err));
  }

  render() {
    return (
      <FeatureProductsWrapper>
        <div className="py-5 feature-products-area">
          <div className="container">
            <Title title="Our Feature products" />
            <div className="row">
              {this.state.products.length > 0 &&
                this.state.products.map(product => (
                  <FeatureProduct product={product} key={product.product_id} />
                ))}
            </div>
          </div>
        </div>
      </FeatureProductsWrapper>
    );
  }
}

const FeatureProductsWrapper = styled.div`
  .feature-products-area {
    background: #f8f9fa;
  }
`;
